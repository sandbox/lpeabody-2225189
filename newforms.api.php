<?php
/**
 * @file
 * API documentation file for NewForms module.
 */

/**
 * Modules can provide their own components to be used in forms.
 * @return array list of components made available by the implementing module
 */
function hook_newforms_components() {
  $components = array();
  $components['my_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('This is the value of my checkbox.'),
  );
  return $components;
}
