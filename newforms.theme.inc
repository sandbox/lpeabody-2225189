<?php
/**
 * @file
 * Theme functions for NewForms module.
 */
function theme_components_list($variables) {
  dpm('snap');
  $components = $variables['components'];
  $output = '';

  foreach ($components as $key => $value) {
    $output .= '<div>' . $value['title'] . '</div>';
  }

  return $output;
}
