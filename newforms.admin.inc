<?php
/**
 * @file
 * Contains admininistration forms and helper functions.
 */

function newforms_administration_settings($form, &$form_state) {
  libraries_load('gridster');

  $form['demo_title'] = array(
    '#markup' => '<h1>Demo</h1>',
  );

  $demo = <<<DEMO
<div class="demo">
  <div class="gridster ready">
    <ul style="height: 480px; width: 960px; position: relative;">
      <li data-row="1" data-col="1" data-sizex="2" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
      <li data-row="3" data-col="1" data-sizex="1" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>

      <li data-row="3" data-col="2" data-sizex="2" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
      <li data-row="1" data-col="3" data-sizex="2" data-sizey="2" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>

      <li class="try gs-w" data-row="1" data-col="5" data-sizex="1" data-sizey="1" data-max-sizex="1" data-max-sizey="1"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
      <li data-row="2" data-col="1" data-sizex="2" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
      <li data-row="3" data-col="4" data-sizex="1" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>

      <li data-row="1" data-col="6" data-sizex="1" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
      <li data-row="3" data-col="5" data-sizex="1" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>

      <li data-row="2" data-col="5" data-sizex="1" data-sizey="1" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
      <li data-row="2" data-col="6" data-sizex="1" data-sizey="2" class="gs-w"><span class="gs-resize-handle gs-resize-handle-both"></span></li>
    </ul>
  </div>
</div>
DEMO;

  $js = <<<JS

var gridster;

(function($){

  gridster = $(".gridster > ul").gridster({
      widget_margins: [10, 10],
      widget_base_dimensions: [140, 140],
      min_cols: 6,
      resize: {
          enabled: true
      }
  }).data('gridster');


  // var collection = [
  //     ['<li><div class="title">drag</div>Widget content</li>', 1, 2],
  //     ['<li><div class="title">drag</div>Widget content</li>', 5, 2],
  //     ['<li><div class="title">drag</div>Widget content</li>', 3, 2],
  //     ['<li><div class="title">drag</div>Widget content</li>', 2, 1],
  //     ['<li><div class="title">drag</div>Widget content</li>', 4, 1],
  //     ['<li><div class="title">drag</div>Widget content</li>', 1, 2],
  //     ['<li><div class="title">drag</div>Widget content</li>', 2, 1],
  //     ['<li><div class="title">drag</div>Widget content</li>', 3, 2],
  //     ['<li><div class="title">drag</div>Widget content</li>', 1, 1],
  //     ['<li><div class="title">drag</div>Widget content</li>', 2, 2],
  //     ['<li><div class="title">drag</div>Widget content</li>', 1, 3],
  // ];

  // $.each(collection, function(i, widget){
  //     gridster.add_widget.apply(gridster, widget)
  // });

})(jQuery);

JS;

  drupal_add_js($js, array('type' => 'inline', 'scope' => 'footer'));

  $form['description'] = array(
    '#markup' => $demo,
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'newforms') . '/styles/admin.css',
  );

  return $form;
}

/**
 * Provide a list of available forms in the system.
 * @return [type]
 */
function newforms_form_list() {
  $render = array();
  $forms = newforms_get_forms();
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => '3'));
  $base = 'admin/config/content/newforms/manage';
  $rows = array();

  foreach ($forms as $fid => $form) {
    $row = array(t($form['title']));
    $row[] = array('data' => l(t('edit form'), "$base/$fid"));
    $row[] = array('data' => l(t('delete form'), "$base/$fid/delete"));
    $row[] = array('data' => l(t('view submissions'), "$base/$fid/submissions"));
    $rows[] = $row;
  }

  $render['form_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No forms available. <a href="@link">Add form</a>.', array('@link' => url('admin/config/content/newforms/add'))),
  );

  return $render;
}

function newforms_components_list($form, &$form_state) {
  $form['newforms_item'] = array(
    '#markup' => '<h1>Components</h1>',
  );

  return $form;
}

function newforms_form($form, &$form_state, $newform = NULL) {

  $form['components'] = array(
    '#theme' => 'components_listt',
    '#element' => array('components' => newforms_get_components()),
  );

  if (isset($newform)) {
    $form["#new"] = FALSE;
    $form['#fid'] = $newform['fid'];
    drupal_set_title('Edit form');
  }
  else {
    $form["#new"] = TRUE;
    drupal_set_title('Create form');
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Example field.'),
    '#default_value' => isset($newform['title']) ? $newform['title'] : '',
    '#size' => 60,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t((isset($newform) ? 'Edit' : 'Create') . ' form'),
    '#weight' => 40,
  );

  return $form;
}

function newforms_form_validate($form, &$form_state) {

}

function newforms_form_submit($form, &$form_state) {
  $record = array();
  $record['form'] = array(
    'title' => $form_state['values']['title'],
  );

  if ($form['#new']) {
    drupal_write_record('newforms_forms', $record);
    drupal_set_message(t('Added form!'), 'status', FALSE);
  }
  else {
    drupal_set_message(t('Updated form!'), 'status', FALSE);
    $record['fid'] = $form['#fid'];
    drupal_write_record('newforms_forms', $record, array('fid'));
  }
}

function newforms_form_delete($form, &$form_state, $newform) {
  $message = t('Are you sure you want to delete the form %form?', array('%form' => $newform['title']));
  $caption = '';
  $caption .= '<p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form, $message, 'admin/config/content/newforms/forms', $caption, t('Delete'));
}

function newforms_form_submissions($form, &$form_state, $newform) {
  drupal_set_title('Submissions');
  $fid = $newform['fid'];
  $form['this'] = array(
    '#markup' => '<p>Hello, World!</p>',
  );

  return $form;
}
