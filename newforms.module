<?php

/**
 * @file
 * Contains module hooks for NewForms module.  Also contains helper functions.
 */

/**
 * Implements hook_permission().
 */
function newforms_permission() {
  return array(
    'administer newforms module' =>  array(
      'title' => t('Administer NewForms module'),
      'description' => t('Perform administration tasks for NewForms module.'),
    ),
  );
}

/**
 * Implements hook_libraries_info().
 * @return array libraries available to load
 */
function newforms_libraries_info() {
  $libraries = array();

  $libraraies['gridster'] = array(
    'name' => 'Gridster',
    'vendor url' => 'https://github.com/ducksboard/gridster.js',
    'download url' => 'https://github.com/ducksboard/gridster.js',
    'path' => 'dist',
    'version arguments' => array(
      'file' => 'package.json',
      'pattern' => '@"version": "([\d.]+)"@',
    ),
    'files' => array(
      'js' => array('jquery.gridster.js'),
      'css' => array('jquery.gridster.css'),
    ),
    'variants' => array(
      'min' => array(
        'files' => array(
          'js' => array('jquery.gridster.min.js'),
          'css' => array('jquery.gridster.min.css'),
        ),
      ),
      'extras' => array(
        'files' => array(
          'js' => array('jquery.gridster.with-extras.js'),
          'css' => array('jquery.gridster.with-extras.css'),
        ),
      ),
    ),
  );

  $libraraies['angular'] = array(
    'name' => 'Angular.js',
    'vendor url' => 'https://github.com/angular/angular.js',
    'download url' => 'https://github.com/angular/angular.js',
    'version arguments' => array(
      'file' => 'version.txt',
      'pattern' => '@(.*)@',
    ),
    'files' => array(
      'js' => array('angular.js'),
    ),
    'variants' => array(
      'min' => array(
        'files' => array(
          'js' => array('angular.min.js'),
        ),
      ),
    ),
  );

  return $libraraies;
}

/**
 * Implements hook_menu().
 */
function newforms_menu() {
  $items = array();
  $items['admin/config/content/newforms'] = array(
    'title' => 'NewForms',
    'description' => 'Configure web forms via drag and drop.',
    'page callback' => 'newforms_form_list',
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  $items['admin/config/content/newforms/forms'] = array(
    'title' => 'Forms',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/config/content/newforms/add'] = array(
    'title' => 'Add Form',
    'type' => MENU_LOCAL_ACTION,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('newforms_form'),
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  $items['admin/config/content/newforms/components'] = array(
    'title' => 'Components',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('newforms_components_list'),
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  $items['admin/config/content/newforms/settings'] = array(
    'title' => 'Demo',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('newforms_administration_settings'),
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  $items['admin/config/content/newforms/manage/%newform'] = array(
    'title' => 'Manage forms',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('newforms_form', 5),
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  $items['admin/config/content/newforms/manage/%newform/edit'] = array(
    'title' => 'Edit',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/config/content/newforms/manage/%newform/delete'] = array(
    'title' => 'Delete',
    'type' => MENU_LOCAL_TASK,
    'page arguments' => array('newforms_form_delete', 5),
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  $items['admin/config/content/newforms/manage/%newform/submissions'] = array(
    'title' => 'Submissions',
    'type' => MENU_LOCAL_TASK,
    'page arguments' => array('newforms_form_submissions', 5),
    'access arguments' => array('administer newforms module'),
    'file' => 'newforms.admin.inc',
  );

  return $items;
}

function newform_load($fid) {
  $result = db_select('newforms_forms', 'nf')
    ->fields('nf')
    ->condition('fid',$fid,'=')
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();
  if (!empty($result)) {
    $result = unserialize($result['form']);
    $result['fid'] = $fid;
  }
  return $result;
}

/**
 * Implements hook_theme().
 */
function newforms_theme($existing, $type, $theme, $path) {
  return array(
    /*
     *'forum_icon' => array(
     *  'variables' => array('new_posts' => NULL, 'num_posts' => 0, 'comment_mode' => 0, 'sticky' => 0),
     *),
     *'status_report' => array(
     *  'render element' => 'requirements',
     *  'file' => 'system.admin.inc',
     *),
     */
    'components_list' => array(
      'render element' => 'components',
      'file' => 'newforms.theme.inc',
    ),
  );
}

function newforms_get_forms() {
  $results = array();
  $result = db_select('newforms_forms', 'nf')
    ->fields('nf')
    ->execute()
    ->fetchAllAssoc('fid');
  foreach ($result as $value) {
    $results[$value->fid] = unserialize($value->form);
  }
  return $results;
}

function newforms_get_components() {
  $components = array();
  // get all defined form components from additional modules
  $components += module_invoke_all('newforms_components');
  return $components;
}

function newforms_newforms_components() {
  $components = array();

  $components['checkbox'] = array(
    'title' => 'Checkbox',
  );

  $components['textfield'] = array(
    'title' => 'Text Field',
  );

  $components['textarea'] = array(
    'title' => 'Text Area',
  );

  $components['radio'] = array(
    'title' => 'Radio Button',
  );

  $components['submit'] = array(
    'title' => 'Submit Button',
  );

  return $components;
}
